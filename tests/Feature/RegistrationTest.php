<?php


namespace Techneved\LaravelAuth\Tests\Feature;

use Composer\Cache;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver;
use Techneved\LaravelAuth\Tests\TestCase;
use Techneved\LaravelAuthVerification\Fake\FakeVerification;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */

    public function user_register_route_is_exist()
    {
        $response = $this->postJson(route('register'));
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function registration_fields_are_required()
    {
        $data = [
            'first_name' => "",
            'last_name' => "",
            'email' => "",
            'phone' => "",
            'password' => ""
        ];
        $response = $this->postJson(route('register'), $data);
        $response->assertStatus(422);
    }

    /** @test */
    public function registration_phone_field_digits_not_equal_to_ten()
    {
        $data = [
            'first_name' => "test",
            'last_name' => "test",
            'email' => "tet@gmail.com",
            'phone' => 1232311,
            'password' => 123456
        ];

        $response = $this->postJson(route('register'), $data);
        $response->assertStatus(422);
    }

    /** @test */
    public function registration_email_field_is_not_email_type()
    {

        $data = [
            'first_name' => "test",
            'last_name' => "test",
            'email' => "email",
            'phone' => 1231232311,
            'password' => 123456
        ];
        $response = $this->postJson(route('register'), $data);

        $response->assertStatus(422);
    }

    /** @test */
    public function registration_email_id_is_already_exist()
    {
        $user = $this->user(['is_active' => false],1);

        $data = [
            'first_name' => "test",
            'last_name' => "test",
            'email' => $user[0]->email,
            'phone' => 1231232311,
            'password' => 123456
        ];
        $response = $this->post(route('register'), $data);

        $response->assertSessionHasErrors();
    }

    /** @test */
    public function registration_phone_no_is_already_exist()
    {
        $user = $this->user(['is_active' => false],1);

        $data = [
            'first_name' => "test",
            'last_name' => "test",
            'email' => "test@gmail.com",
            'phone' => $user[0]->phone,
            'password' => 123456
        ];
        $response = $this->postJson(route('register'), $data);

        $response->assertStatus(422);
    }

    /**
     * @test
     *
     * @throws \Exception
     */
    public function user_can_successfully_registered()
    {
        $data = [
            'first_name' => "Amish",
            'last_name' => "Khurana",
            'email' => "test@gmail.com",
            'phone' => '1234567890',
            'password' => 123456
        ];

        $response = $this->postJson(route('register'), $data);

        $this->assertEqualsCanonicalizing(cache('user_registration'), $data);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function otp_is_required_for_verification()
    {
        $data = [
            'first_name' => "Amish",
            'last_name' => "Khurana",
            'email' => "test@gmail.com",
            'phone' => '1234567890',
            'password' => 123456
        ];

        $this->postJson(route('register'), $data);
        $response = $this->postJson(route('verify-otp'), ['receiver' => $data['phone']]);
        $response->assertStatus(422);

    }

    /**
     * @test
     */
    public function otp_is_invalid_for_verification()
    {
        $data = [
            'first_name' => "Amish",
            'last_name' => "Khurana",
            'email' => "test@gmail.com",
            'phone' => '1234567890',
            'password' => 123456
        ];

        $this->postJson(route('register'), $data);
        $otp_data = [
            'receiver' => $data['phone'],
            'otp' => 123456
        ];
        $response = $this->postJson(route('verify-otp'), $otp_data);
        $response->assertStatus(422);

    }

    /**
     * @test
     * @throws \Exception
     */
    public function otp_is_successfully_verified_for_verification()
    {
        $data = [
            'first_name' => "Amish",
            'last_name' => "Khurana",
            'email' => "test@gmail.com",
            'phone' => '1234567890',
            'password' => 123456
        ];
        $this->assertTrue(true);
        $this->postJson(route('register'), $data);

        $otp_data = [
            'receiver' => $data['phone'],
            'otp' => FakeVerification::getOTP($data['phone'])['otp']
        ];

        $verification_response = $this->postJson(route('verify-otp'), $otp_data);

        $verification_response->assertStatus(200);

        $this->assertNull(cache('user_registration'));

    }
    
}