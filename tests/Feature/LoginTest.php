<?php

namespace Techneved\LaravelAuth\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */

    public function user_login_route_is_exist()
    {
        $response = $this->postJson(route('login'));
        $response->assertSessionHasNoErrors();
    }


    /** @test */

    public function user_can_get_error_for_required_credentials()
    {
        $data = [
            'email' => '',
            'password' => '',
        ];
        $response = $this->post(route('login'), $data);
        $response->assertSessionHasErrors();
    }

    /** @test */

    public function user_can_login_with_invalid_phone_password_credentials()
    {
        $data = [
            'email' => "17545342345",
            'password' => '123456',
        ];
        $response = $this->post(route('login'), $data);
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'errors' => [
                    'error' => [trans("laravel-auth::laravel-auth.invalid_credentials")]
                ]
            ]);
    }

    /** @test */

    public function user_can_login_with_invalid_email_password_credentials()
    {
        $data = [
            'email' => "test@gmail.com",
            'password' => '123456',
        ];
        $response = $this->post(route('login'), $data);
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'errors' => [
                    'error' => [trans("laravel-auth::laravel-auth.invalid_credentials")]
                ]
            ]);
    }

    /** @test */

    public function user_is_not_active()
    {
        $user = $this->user(['is_active' => false], 1);
        $data = [
            'email' => $user[0]->email,
            'password' => 'password',
        ];
        $response = $this->postJson(route('login'), $data);
        $response
            ->assertStatus(422)
            ->assertJsonFragment([
                'errors' => [
                    'error' => [trans("laravel-auth::laravel-auth.invalid_credentials")]
                ]
            ]);
    }

    /** @test */

    public function user_is_active()
    {
        $user = $this->user(['is_active' => true], 1);

        $data = [
            'email' => $user[0]->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);
        $response->assertStatus(200);

        $this->assertNotNull($response['access_token']);;

        $this->assertTrue($this->auth->check());
    }

    /** @test */

    public function user_email_is_not_verified()
    {
        $user = $this->user(['email_verified_at' => ''], 1);

        $data = [
            'email' => $user[0]->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);
        $response
            ->assertStatus(403)
            ->assertJsonFragment([
                'errors' => [
                    'error' =>  ['Email id ' . trans('laravel-auth::laravel-auth.not_verified')],
                    'status' => config('laravel-auth.status.verification_email')
                ]
            ]);
    }

    /** @test */

    public function user_email_is_verified()
    {
        $user = $this->user([], 1);

        $data = [
            'email' => $user[0]->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);

        $response->assertStatus(200);

        $this->assertNotNull($response['access_token']);;

        $this->assertTrue($this->auth->check());
    }

    /** @test */

    public function user_phone_is_not_verified()
    {
        $user = $this->user(['phone_verified_at' => ''], 1);

        $data = [
            'email' => $user[0]->phone,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);

        $response
            ->assertStatus(403)
            ->assertJsonFragment([
                'errors' => [
                    'error' =>  ['Phone No. ' . trans('laravel-auth::laravel-auth.not_verified')],
                    'status' => config('laravel-auth.status.verification_phone')
                ]
            ]);
    }

    /** @test */

    public function user_phone_is_verified()
    {
        $user = $this->user([], 1);

        $data = [
            'email' => $user[0]->phone,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);

        $response->assertStatus(200);

        $this->assertNotNull($response['access_token']);;

        $this->assertTrue($this->auth->check());
    }


    /** @test */

    public function user_can_login_with_phone_credentials()
    {
        $user = $this->user([], 1);
        $data = [
            'email' => $user[0]->phone,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);

        $response->assertStatus(200);

        $this->assertNotNull($response['access_token']);;

        $this->assertTrue($this->auth->check());
    }

    /** @test */

    public function user_can_login_with_email_credentials()
    {
        $user = $this->user([], 1);
        $data = [
            'email' => $user[0]->email,
            'password' => 'password',
        ];

        $response = $this->postJson(route('login'), $data);

        $response->assertStatus(200);

        $this->assertNotNull($response['access_token']);;

        $this->assertTrue($this->auth->check());
    }

    
}
