<?php


namespace Techneved\LaravelAuth\Tests\Feature;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Techneved\LaravelAuth\Tests\TestCase;

class VerificationTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function send_verification_link_through_email_route()
    {
        $response = $this->postJson(route('send-verification'));
        $response->assertSessionHasNoErrors();
    }

    /** @test */


    public function email_is_required()

    {
        $data = [
            'email' => ''
        ];

        $response = $this->post(route('send-verification'), $data);

        $response->assertSessionHasErrors();
    }
}