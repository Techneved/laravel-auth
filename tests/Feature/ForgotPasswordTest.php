<?php


namespace Techneved\LaravelAuth\Tests;


use Illuminate\Foundation\Testing\RefreshDatabase;

class ForgotPasswordTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function phone_verify_route_is_exist()
    {
        $response = $this->postJson(route('phone-verify'));
        $response->assertSessionHasNoErrors();
    }

    /** @test */
    public function phone_number_is_required()
    {

        $data = ['phone' => ''];
        $response = $this->post(route('phone-verify'), $data);
        $response->assertSessionHasErrors();

    }


}