<?php

namespace Techneved\LaravelAuth\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function testing_user_migration_database()
    {
        $user = $this->user([], 1);
        $this->assertCount(1, $user);
    }
}