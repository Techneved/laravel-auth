<?php
return [

    /*
   |--------------------------------------------------------------------------
   | Authentication Language Lines
   |--------------------------------------------------------------------------
   |
   | The following language lines are used during authentication for various
   | messages that we need to display to the user. You are free to modify
   | these language lines according to your application's requirements.
   |
   */


    'invalid_credentials' => 'These credentials do not match our records',
    'not_verified' => 'is not verified',
    'successfully_registered' => 'User is successfully registered',
    'otp_successfully' => 'Otp successfully sent',
    'internal_error' => 'Something wend wrong'

];