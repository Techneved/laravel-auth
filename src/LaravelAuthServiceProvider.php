<?php


namespace Techneved\LaravelAuth;


use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class LaravelAuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerResource();
        $this->registerRoutes();

        if ($this->app->runningInConsole()) {

            $this->registerPublishing();
        }
    }

    public function register()
    {

        $this->mergeConfiguration();
    }

    /**
     * Register the package resources
     *
     * @return void
     */
    private function registerResource()
    {

        /** Load migration from package */
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        /** Load migration for auth-verification */
        $this->loadMigrationsFrom(__DIR__.'/../vendor/techneved/auth-verification/database/migrations');

        /** Load translation from package */
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang','laravel-auth');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-auth');

    }

    /**
     * Register the package routes
     *
     * @return void
     */
    private function registerRoutes()
    {

        Route::group($this->routeConfig(), function () {

            $this->loadRoutesFrom(__DIR__.'/../routes/api.php');
        });
    }

    /**
     * Creating configuration of route's
     *
     * @return array
     */
    private function routeConfig()
    {
        $config = $this->app->config['laravel-auth'];

        return  [
            'prefix' => 'api/'.$config['prefix'],
            'namespace' => 'Techneved\LaravelAuth\HTTP\Controllers'
        ];
    }

    /**
     * Publishing package's files
     *
     * @return void
     */

    public function registerPublishing()
    {
        /** publishing the configuration of laravel auth to package*/
        $this->publishes([
            __DIR__.'/../config/laravel-auth.php' => config_path('laravel-auth.php')
        ], 'laravel-auth-config');

        /** publishing the translation of laravel auth to package*/

        $this->publishes([
            __DIR__.'/../resources/lang/' => resource_path('lang/'),
        ],'laravel-auth-translation');

    }

    /**
     * Merge configuration file of service provider
     *
     * @return void
     */
    protected function mergeConfiguration()
    {
        /** Merging configuration file to the package */
        $this->mergeConfigFrom(
            __DIR__.'/../config/laravel-auth.php', 'laravel-auth'
        );

        /** Merging auth file to the package */
        $this->mergeAuthFileFrom(
            __DIR__.'/../config/auth.php', 'auth'
        );
    }

    /**
     * Merge package auth file to original laravel auth file
     *
     * @param $path
     * @param $key
     */
    protected function mergeAuthFileFrom($path, $key)
    {
        $original = $this->app['config']->get($key, []);
        $this->app['config']->set($key, $this->multi_array_merge(require $path, $original));
    }

    /**
     * Merge multiple value in auth array
     *
     * @param $toMerge
     * @param $original
     * @return array
     */
    protected function multi_array_merge($toMerge, $original)
    {
        $auth = [];
        foreach ($original as $key => $value) {
            if (isset($toMerge[$key])) {
                $auth[$key] = array_merge($value, $toMerge[$key]);
            } else {
                $auth[$key] = $value;
            }
        }
        return $auth;
    }


}