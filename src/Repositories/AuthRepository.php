<?php


namespace Techneved\LaravelAuth\Repositories;

use Techneved\LaravelAuth\Models\User;

class AuthRepository
{
    /**
     * User is active
     *
     * @param $user
     * @return mixed
     */
    public function isActive($user)
    {
        return $user->is_active;
    }

    /**
     * Email is verified
     *
     * @param $user
     * @return bool
     */
    public function isEmailVerified($user) {
        
        return $user->email_verified_at != '' ? true : false ;
    }

    /**
     * Phone is verified
     *
     * @param $user
     * @return bool
     */
    public function isPhoneVerified($user) {

        return $user->phone_verified_at != '' ? true : false ;
    }

    public function store($data)
    {
        return User::create($data);
    }
}