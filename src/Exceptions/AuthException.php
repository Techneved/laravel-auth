<?php


namespace Techneved\LaravelAuth\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthException extends HttpException
{
    /**
     * Invalid credentials error message
     *
     * @return array|\Illuminate\Contracts\Translation\Translator|string|null
     */
    public static function invalidCredentials()
    {
        $message = trans('laravel-auth::laravel-auth.invalid_credentials');
        return $message;
    }

    /**
     * Credential is not verified error message
     */

    public static function credentialNotVerified($type)
    {
        $message = trans('laravel-auth::laravel-auth.not_verified');
        return $type.' '.$message;
    }

    /**
     * Credential is not verified error message
     */

    public static function InternalError()
    {
        $message = trans('laravel-auth::laravel-auth.internal_error');
        return $message;
    }

}