<?php


namespace Techneved\LaravelAuth\HTTP\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Techneved\LaravelAuth\Models\User;
use Techneved\LaravelAuthVerification\Models\Verification;

class ForgotPasswordController extends Controller
{
    public function verifyPhone(Request $request)
    {
        $this->verifyPhoneValidations($request);

        $user = User::where('phone', $request->phone)
                      ->first();
        if ($user) {
            $data = $request->all();
            $data['otp'] = Verification::generateOtp();
            Verification::storeUpdate($data);
        }
        return response()->json($user);
    }

    /**
     * Validation of user
     *
     * @param Request $request
     * @return void
     */
    private function verifyPhoneValidations (Request $request)
    {
        $request->validate([
            'phone' => 'required',
        ]);
    }
}