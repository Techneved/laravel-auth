<?php

namespace Techneved\LaravelAuth\HTTP\Controllers;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use \Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Techneved\LaravelAuth\Exceptions\AuthException;
use Techneved\LaravelAuth\Repositories\AuthRepository;

class LoginController extends Controller
{
    /** @var AuthRepository  */
    protected $auth_repository;

    /**
     * LoginController constructor.
     *
     */
    public function __construct()
    {
        $this->auth_repository = new AuthRepository();
    }

    public function login(Request $request)
    {

       $this->validations($request);

       $input = $request->only('email', 'password');

       if (is_numeric($input['email'])) {

           return $this->attemptLogin(['phone' => $input['email'], 'password' => $input['password'], 'is_active' => true]);
       }

       return $this->attemptLogin(['email' => $input['email'], 'password' => $input['password'], 'is_active' => true]);
    }

    /**
     * Validation of user
     *
     * @param Request $request
     * @return void
     */
    private function validations (Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
    }

    /**
     * Attempt user login
     *
     * @param $inputs
     * @return object
     */
    private function attemptLogin($inputs)
    {
        if ($token = $this->credentials($inputs)) {

            if ($this->auth_repository->isActive($this->guard()->user())) {

                return $this->credentialVerified($inputs) ? $this->credentialVerified($inputs) : $this->loginSuccessResponse($token);
            }
        }

        return $this->loginFailedResponse([
            'errors'=> [
                'error' => [AuthException::invalidCredentials()]
            ]
        ],422);
    }

    /**
     * Credential is verification
     *
     * @param $input
     * @return JsonResponse|void
     */
    private function credentialVerified($input)
    {
        if (array_key_exists('email',$input) && config('laravel-auth.verified.email')) {

           if (!$this->auth_repository->isEmailVerified($this->guard()->user())) {

            
               return $this->loginFailedResponse([
                   'errors'=> [
                       'error' => [AuthException::credentialNotVerified('Email id')],
                       'status' => config('laravel-auth.status.verification_email')
                   ]
               ],403);
           }

        }

        if (array_key_exists('phone',$input) && config('laravel-auth.verified.phone')) {

            if (!$this->auth_repository->isPhoneVerified($this->guard()->user())) {

                return $this->loginFailedResponse([
                    'errors' => [
                        'error' => [AuthException::credentialNotVerified('Phone No.')],
                        'status' => config('laravel-auth.status.verification_phone')
                    ]
                ], 403);
            }
        }

        return;

    }

    /**
     * Credential Verification
     *
     * @param array $credentials
     *
     * @return object
     */
    private function credentials(array $credentials)
    {
        return $this->guard()->attempt($credentials);
    }

    /**
     * Login success response
     *
     * @param $token
     * @return mixed
     */
    private function loginSuccessResponse($token)
    {
        return response()->json([
            'data'=> $this->guard()->user(),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $this->guard()->factory()->getTTL() * 60
        ],200);
    }

    /**
     * Login failed response
     *
     * @param $message
     * @param $status
     * @return mixed
     */
    private function loginFailedResponse($message, $status)
    {
        return response()->json(
          $message
        ,$status);
    }

    /**
     *  Auth guard
     *
     * @return object
     */
    private function guard()
    {
        return Auth::guard('api-user');
    }
}