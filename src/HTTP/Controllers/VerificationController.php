<?php

namespace Techneved\LaravelAuth\HTTP\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class VerificationController extends Controller
{
    public function sendVerification(Request $request)
    {
        $message = [
            'required' => 'field is required'
        ];

        $this->validations($request, $message);

        if (is_numeric($request->email)) {

        }
    }

    /**
     * Validation of inputs
     *
     * @param Request $request
     * @param $message
     */
    private function validations (Request $request, $message)
    {
        $request->validate([
            'email' => 'required',
        ], $message);
    }

}