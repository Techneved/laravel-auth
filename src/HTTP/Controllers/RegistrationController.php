<?php


namespace Techneved\LaravelAuth\HTTP\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Techneved\LaravelAuth\Exceptions\AuthException;
use Techneved\LaravelAuth\Models\User;
use Techneved\LaravelAuth\Repositories\AuthRepository;
use Techneved\LaravelAuthVerification\Models\Verification;

class RegistrationController extends Controller
{
    /** @var  */
    protected $auth_repository;

    /**
     * RegistrationController constructor.
     */
    public function __construct()
    {
        $this->auth_repository = new AuthRepository();
    }


    /**
     * Registration of user and create otp
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function register(Request $request)
    {

        $this->validations($request);

        $input = $request->all();

        try {

            cache(['user_registration' => $input], 5);

            Verification::storeUpdate(['receiver' => $request->phone]);

            return response()->json([
                'message' => trans('laravel-auth::laravel-auth.otp_successfully')
            ],200);
        }
        catch (\Exception $exception) {
            return $this->registerFailedResponse(['errors' => [

                'error' => [AuthException::InternalError()],
            ]], 500);
        }
    }

    /**
     * Verification of otp for registration
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyOtp(Request $request)
    {
        $request->validate([
            'otp' => 'required'
        ]);

        try {

            $result = Verification::verify($request->all());

            if (!$result['status']) {

                return $this->registerFailedResponse([
                    'errors' => [
                        'error' => [$result['message']]
                    ]
                ], 422);
            }
            $request_data = cache('user_registration');

            $request_data['password'] = bcrypt($request_data['password']);

            $user = User::create($request_data);

            cache()->forget('user_registration');

            return $this->registerSuccessResponse($user);

        }
        catch (\Exception $exception) {
            return $this->registerFailedResponse(['errors' => [

                'error' => [AuthException::InternalError()],
            ]], 500);
        }


    }

    /**
     * Validation of user
     *
     * @param Request $request
     * @return void
     */
    private function validations (Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required | email | unique:users,email',
            'phone' => 'required | digits:10 | unique:users,phone',
            'password' => 'required',
        ]);
    }

    /**
     * User successfully registered
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    private function registerSuccessResponse(User $user)
    {
        return response()->json([
            'data' => $user,
            'message' => trans('laravel-auth::laravel-auth.successfully_registered')
        ], 200);
    }

    /**
     * registration fail response
     * @param $error
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    private function registerFailedResponse($error, $status)
    {
        return response()->json($error, $status);
    }
}