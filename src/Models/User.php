<?php

namespace Techneved\LaravelAuth\Models;
use \Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    /** @var array  */
    protected $fillable = ['first_name', 'last_name', 'email', 'phone', 'password','is_active','email_verified_at','phone_verified_at'];
 
   /** @var array */
    protected $hidden = ['password'];

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    

}