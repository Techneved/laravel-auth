<?php

use Illuminate\Support\Str;
use Techneved\LaravelAuth\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class,function (Faker\Generator  $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt("password"), // password
        'phone' => $faker->numberBetween(1000000000,2147483647),
        'phone_verified_at' => now(),
        'is_active' => true,
    ];
});