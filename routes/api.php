<?php

Route::post('login','LoginController@login')->name('login');

Route::post('register','RegistrationController@register')->name('register');

Route::post('phone-verify','ForgotPasswordController@VerifyPhone')->name('phone-verify');

Route::post('verify-otp', 'RegistrationController@verifyOtp')->name('verify-otp');

Route::post('send-verification', 'VerificationController@sendVerification')->name('send-verification');

