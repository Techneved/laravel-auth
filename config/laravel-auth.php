<?php
return [

    /**
     * We can define the prefix of route here
     *
     * By default it admin
     */
    'prefix' => "admin",

    /**
     * Email or phone verification is compulsory or not we can define here
     *
     * By default they are true
     */
    'verified' => [
        'email' => true,
        'phone' => true,
    ],

    /**
     * Status for frontend process like email or phone verification
     *
     * By default email has 101 and phone 102
     */
    'status' => [
        'verification_email' => 101,
        'verification_phone' => 102,
    ]

];