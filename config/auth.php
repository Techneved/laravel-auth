<?php
return [
    'guards' => [
        'api-user' => [
            'driver'   => 'jwt',
            'provider' => 'api-users',
        ],
    ],
    'providers' => [
        'api-users' => [
            'driver' => 'eloquent',
            'model'  => \Techneved\LaravelAuth\Models\User::class,
        ],
    ],

];